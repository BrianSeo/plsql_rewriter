﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;
using System.Text.RegularExpressions;

namespace analyzer
{

    public class BkListener : PlSqlBaseListener
    {
        PlSqlParser parser;
        TokenStreamRewriter rewriter;

        public ContextInfo current_ctx_info;

        List<string> listOfStrings = new List<string> {
            "SYSDATE",
            "TO_CHAR(SYSDATE,'YYYYMMDDHH24MI')",
            "'YYYYMMDDHH24MI'",
            "'YYYYMMDD'",
            "SQLERRM",
            "ROWNUM",
            "NULL",
            "SQL%ROWCOUNT",
            "0", "1", "2", "3", "4", "5"
        };

        List<string> ignorableSql_statement = new List<string> {
            "COMMIT"
        };

        public BkListener(PlSqlParser parser, TokenStreamRewriter rewriter)
        {
            this.parser = parser;
            this.rewriter = rewriter;
            ContextInfo ctx_info = new ContextInfo("Root");
            current_ctx_info = ctx_info;
        }

        public void LogContext(ParserRuleContext context, bool show_content = false, bool is_exit = false)
        {

            string content = show_content == true ? context.GetText() : string.Empty;

            Console.WriteLine("{0}-{1} => {2}", 
                is_exit == false ? "Enter" : "Exit",
                parser.RuleNames[context.RuleIndex], 
                content);
        }

        /// <summary>
        /// create new context and set current_ctx_info to created one.
        /// </summary>
        /// <param name="step_name"></param>
        public ContextInfo pushCtxChild(string step_name, ParserRuleContext context)
        {
           
            ContextInfo ctx_info = new ContextInfo(step_name);
            ctx_info.ctx_text = context.GetText();

            ctx_info.parent = this.current_ctx_info;
            this.current_ctx_info.children.Add(ctx_info);
            this.current_ctx_info = ctx_info;

            Console.WriteLine("==> Context switched to : {0} -> {1}", ctx_info.parent.ctx_stmt, ctx_info.ctx_stmt);

           
            return this.current_ctx_info;
        }

        public void popCtxChild()
        {
            Console.WriteLine("<== Context switched back to : {0} -> {1}", current_ctx_info.ctx_stmt, this.current_ctx_info.parent.ctx_stmt);
            this.current_ctx_info = this.current_ctx_info.parent;
        }


        private bool is_ignorable_expr(ParserRuleContext context)
        {
            string expr = context.GetText();
            bool is_ignorable = listOfStrings.Any(expr.Contains);

            return is_ignorable;
        }

        private bool is_ignorable_variable(ParserRuleContext context)
        {
            ContextInfo ctx_info = this.current_ctx_info;
            bool is_ignorable = false;
            while (ctx_info != null)
            {
                // Console.WriteLine(ctx_info.ctx_stmt);
                is_ignorable = ctx_info.variables.Any(context.GetText().Contains);
                if (is_ignorable)
                {
                    break;
                }
                ctx_info = ctx_info.parent;
            }

            return is_ignorable;

        }

        private bool is_quoted_string(ParserRuleContext context)
        {
            Regex regexString = new Regex(@"'[^']*'");
            string item = context.GetText();
            return regexString.IsMatch(item);

        }


        private bool is_target_str(ParserRuleContext context)
        {
            Regex regexString = new Regex(@"^[a-zA-Z0-9\\_]+.[a-zA-Z0-9\\_]+$");
            string item = context.GetText();
            return regexString.IsMatch(item);

        }

        /// <summary>
        /// just column name
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        // 
        private bool is_simple_name(ParserRuleContext context)
        {
            Regex regexString = new Regex(@"^[a-zA-Z0-9\\_]+$");
            string item = context.GetText();
            return regexString.IsMatch(item);

        }

        public override void EnterSql_statement([NotNull] PlSqlParser.Sql_statementContext context)
        {
            string expr = context.GetText();
            bool is_ignorable = ignorableSql_statement.Any(expr.Contains);
            if (!is_ignorable)
            {
                // Console.WriteLine("EnterSql_statement : {0}", context.GetText());
                LogContext(context);
                this.pushCtxChild("Sql_statement", context);
            }

            base.EnterSql_statement(context);
        }

        public override void ExitSql_statement([NotNull] PlSqlParser.Sql_statementContext context)
        {
            string expr = context.GetText();
            bool is_ignorable = ignorableSql_statement.Any(expr.Contains);
            if (!is_ignorable)
            {
                // Console.WriteLine("ExitSql_statement : {0}", context.GetText());
                this.popCtxChild();
            }

            base.ExitSql_statement(context);
        }


        public override void EnterSelect_statement([NotNull] PlSqlParser.Select_statementContext context)
        {
            // Console.WriteLine("EnterSelect_statement : {0}", context.GetText());
            LogContext(context);
            this.pushCtxChild("Select_statement", context);

            base.EnterSelect_statement(context);
        }

        public override void ExitSelect_statement([NotNull] PlSqlParser.Select_statementContext context)
        {
            // Console.WriteLine("ExitSelect_statement");
            LogContext(context, false, true);
            this.popCtxChild();

            base.ExitSelect_statement(context);
        }


        public override void EnterSubquery([NotNull] PlSqlParser.SubqueryContext context)
        {
            // Console.WriteLine("EnterSubquery : {0}", context.GetText());
            LogContext(context);
            this.pushCtxChild("EnterSubquery", context);

            base.EnterSubquery(context);
        }

        public override void ExitSubquery([NotNull] PlSqlParser.SubqueryContext context)
        {
            // Console.WriteLine("ExitSubquery");
            LogContext(context, false, true);
            this.popCtxChild();

            base.ExitSubquery(context);
        }
        public override void EnterSelect_list_elements([NotNull] PlSqlParser.Select_list_elementsContext context)
        {
            // Console.WriteLine("EnterSelect_list_elements : {0}", context.GetText());
            base.EnterSelect_list_elements(context);
        }

 
        public override void ExitSelect_list_elements([NotNull] PlSqlParser.Select_list_elementsContext context)
        {
            // Console.WriteLine("ExitSelect_list_elements : {0}", context.GetText());
            base.ExitSelect_list_elements(context);
        }


        public override void EnterSelected_element([NotNull] PlSqlParser.Selected_elementContext context)
        {
            LogContext(context, true);
            var selem = new SelectedElement();
            selem.selected_element = context;
            selem.expressions = new List<ParserRuleContext>();
            this.current_ctx_info.selected_elements.Add(selem);

            base.EnterSelected_element(context);

            // next -> EnterExpression
        }

        public override void ExitSelected_element([NotNull] PlSqlParser.Selected_elementContext context)
        {
            LogContext(context, true, true);
            base.ExitSelected_element(context);
        }

        public override void EnterTable_ref([NotNull] PlSqlParser.Table_refContext context)
        {
            // Console.WriteLine("EnterTable_ref : {0}", context.GetText());
            base.EnterTable_ref(context);
        }

        public override void EnterTable_ref_aux([NotNull] PlSqlParser.Table_ref_auxContext context)
        {
            // Console.WriteLine("EnterTable_ref_aux : {0}", context.GetText());
            base.EnterTable_ref_aux(context);
        }

        public override void EnterTable_ref_list([NotNull] PlSqlParser.Table_ref_listContext context)
        {
            // Console.WriteLine("EnterTable_ref_list : {0}", context.GetText());
            base.EnterTable_ref_list(context);
        }



        public override void EnterTableview_name([NotNull] PlSqlParser.Tableview_nameContext context)
        {
            // tablename is registered on EnterDml_table_expression_clause function
            base.EnterTableview_name(context);
        }

        public override void ExitTableview_name([NotNull] PlSqlParser.Tableview_nameContext context)
        {
            base.ExitTableview_name(context);
        }
        public override void EnterTable_alias([NotNull] PlSqlParser.Table_aliasContext context)
        {
            // Console.WriteLine("EnterTable_alias : {0}", context.GetText());
            LogContext(context, true);

            DmlTable table = this.current_ctx_info.tables[this.current_ctx_info.tables.Count - 1] as DmlTable;
            table.table_alias = context;

            base.EnterTable_alias(context);
        }

        public override void ExitTable_alias([NotNull] PlSqlParser.Table_aliasContext context)
        {
            // ignorable.
            base.ExitTable_alias(context);
        }

        public override void ExitTable_ref([NotNull] PlSqlParser.Table_refContext context)
        {
            // Console.WriteLine("ExitTable_ref : {0}", context.GetText());

            base.ExitTable_ref(context);
        }

        public override void EnterPackage_name([NotNull] PlSqlParser.Package_nameContext context)
        {
            LogContext(context, true);
            base.EnterPackage_name(context);
        }

        public override void EnterProcedure_name([NotNull] PlSqlParser.Procedure_nameContext context)
        {
            LogContext(context);
            
            base.EnterProcedure_name(context);
        }

        public override void EnterParameter_name([NotNull] PlSqlParser.Parameter_nameContext context)
        {
            
            if (!is_ignorable_variable(context))
            {
                // Console.WriteLine("EnterParameter_name : {0}", context.GetText());
                LogContext(context);
                this.current_ctx_info.variables.Add(context.GetText()); 
            }  
            
            base.EnterParameter_name(context);
        }

        public override void ExitProcedure_name([NotNull] PlSqlParser.Procedure_nameContext context)
        {
            // Console.WriteLine("ExitProcedure_name : {0}", context.GetText());
            base.ExitProcedure_name(context);
        }

        public override void EnterProcedure_spec([NotNull] PlSqlParser.Procedure_specContext context)
        {
            // Console.WriteLine("EnterProcedure_spec : {0}", context.GetText());
            ContextInfo ctx_proc = this.pushCtxChild("EnterProcedure_spec", context);

            base.EnterProcedure_spec(context);
        }

        public override void ExitProcedure_spec([NotNull] PlSqlParser.Procedure_specContext context)
        {
            // Console.WriteLine("ExitProcedure_spec : {0}", context.GetText());
            this.popCtxChild();
            base.ExitProcedure_spec(context);
        }

        public override void EnterProc_decl_in_type([NotNull] PlSqlParser.Proc_decl_in_typeContext context)
        {
            // Console.WriteLine("EnterProc_decl_in_type : {0}", context.GetText());
            LogContext(context);
            base.EnterProc_decl_in_type(context);
        }

        public override void ExitProc_decl_in_type([NotNull] PlSqlParser.Proc_decl_in_typeContext context)
        {
            // Console.WriteLine("ExitProc_decl_in_type : {0}", context.GetText());
            LogContext(context, false, true);
            base.ExitProc_decl_in_type(context);
        }


        public override void EnterColumn_name([NotNull] PlSqlParser.Column_nameContext context)
        {
            // Console.WriteLine("EnterColumn_name : {0}", context.GetText());
            LogContext(context, true);
            this.current_ctx_info.columns.Add(context);
            
            base.EnterColumn_name(context);
        }

        public override void ExitColumn_name([NotNull] PlSqlParser.Column_nameContext context)
        {
            base.ExitColumn_name(context);
        }



        public override void EnterColumn_alias([NotNull] PlSqlParser.Column_aliasContext context)
        {
            LogContext(context, true);
            // Console.WriteLine("EnterColumn_alias : {0}", context.GetText());
            base.EnterColumn_alias(context);
        }

        public override void ExitColumn_alias([NotNull] PlSqlParser.Column_aliasContext context)
        {
            // Console.WriteLine("ExitColumn_alias : {0}", context.GetText());
            base.ExitColumn_alias(context);
        }

        public override void EnterExpression_list([NotNull] PlSqlParser.Expression_listContext context)
        {
            // Console.WriteLine("EnterExpression_list : {0}", context.GetText());
            base.EnterExpression_list(context);
        }

        public override void EnterAdditive_expression([NotNull] PlSqlParser.Additive_expressionContext context)
        {
            LogContext(context, true);
            this.current_ctx_info.expr_fields.Add(context);

            //if (!is_quoted_string(context))
            //{
            //    if (!is_ignorable_expr(context))
            //    {
            //        if (!is_ignorable_variable(context))
            //        {
            //            if (is_target_str(context)) { 
            //                // Console.WriteLine("EnterAdditive_expression : {0}", context.GetText());
            //                LogContext(context, true);
            //                this.current_ctx_info.expr_fields.Add(context);
            //            }
            //        }
            //    }
            //}

            base.EnterAdditive_expression(context);
        }

        public override void ExitAdditive_expression([NotNull] PlSqlParser.Additive_expressionContext context)
        {
            base.ExitAdditive_expression(context);
        }

        public override void EnterDml_table_expression_clause([NotNull] PlSqlParser.Dml_table_expression_clauseContext context)
        {
            base.EnterDml_table_expression_clause(context);
            if (context.GetText().Equals("DUAL")) return;

            Console.WriteLine("EnterDml_table_expression_clause : {0}", context.GetText());
            DmlTable table = new DmlTable();
            table.table_name = context;

            this.current_ctx_info.tables.Add(table);

            
        }

        public override void ExitDml_table_expression_clause([NotNull] PlSqlParser.Dml_table_expression_clauseContext context)
        {
            // Console.WriteLine("ExitDml_table_expression_clause : {0}", context.GetText());
            base.ExitDml_table_expression_clause(context);
        }

        public override void EnterCondition_wrapper([NotNull] PlSqlParser.Condition_wrapperContext context)
        {
            // Console.WriteLine("EnterCondition_wrapper : {0}", context.GetText());
            base.EnterCondition_wrapper(context);
        }

        public override void ExitCondition_wrapper([NotNull] PlSqlParser.Condition_wrapperContext context)
        {
            // Console.WriteLine("ExitCondition_wrapper : {0}", context.GetText());
            base.ExitCondition_wrapper(context);
        }

        public override void EnterAggregate_function_name([NotNull] PlSqlParser.Aggregate_function_nameContext context)
        {
            Console.WriteLine("EnterAggregate_function_name : {0}", context.GetText());
            base.EnterAggregate_function_name(context);
        }

        public override void EnterBoolean_static_expression([NotNull] PlSqlParser.Boolean_static_expressionContext context)
        {
            Console.WriteLine("EnterBoolean_static_expression : {0}", context.GetText());
            base.EnterBoolean_static_expression(context);
        }

        public override void EnterCursor_declaration([NotNull] PlSqlParser.Cursor_declarationContext context)
        {
            Console.WriteLine("EnterCursor_declaration : {0}", context.GetText());
            base.EnterCursor_declaration(context);
        }

        public override void EnterInsert_statement([NotNull] PlSqlParser.Insert_statementContext context)
        {
            // Console.WriteLine("EnterInsert_statement : {0}", context.GetText());
            base.EnterInsert_statement(context);
        }

        public override void EnterWhere_clause([NotNull] PlSqlParser.Where_clauseContext context)
        {
            // Console.WriteLine("EnterWhere_clause : {0}", context.GetText());
            base.EnterWhere_clause(context);
        }

        public override void EnterGroup_by_clause([NotNull] PlSqlParser.Group_by_clauseContext context)
        {
            // Console.WriteLine("EnterGroup_by_clause : {0}", context.GetText());
            LogContext(context, true);
            base.EnterGroup_by_clause(context);
        }

        public override void EnterInsert_into_clause([NotNull] PlSqlParser.Insert_into_clauseContext context)
        {
            // Console.WriteLine("EnterInsert_into_clause : {0}", context.GetText());
            LogContext(context, true);
            base.EnterInsert_into_clause(context);
        }

        public override void EnterUpdate_statement([NotNull] PlSqlParser.Update_statementContext context)
        {
            // Console.WriteLine("EnterUpdate_statement : {0}", context.GetText());
            LogContext(context);
            this.pushCtxChild("EnterUpdate_statement", context);
            // var token = context.GetTokens(421); // 'UPDATE'
            base.EnterUpdate_statement(context);
        }

        public override void ExitUpdate_statement([NotNull] PlSqlParser.Update_statementContext context)
        {
            this.popCtxChild();
            base.ExitUpdate_statement(context);
        }

        public override void EnterDelete_statement([NotNull] PlSqlParser.Delete_statementContext context)
        {
            // Console.WriteLine("EnterDelete_statement : {0}", context.GetText());
            LogContext(context);
            base.EnterDelete_statement(context);
        }

        public override void EnterCondition([NotNull] PlSqlParser.ConditionContext context)
        {
            Console.WriteLine("EnterCondition : {0}", context.GetText());
            base.EnterCondition(context);
        }



        public override void EnterAlias_quoted_string([NotNull] PlSqlParser.Alias_quoted_stringContext context)
        {
            Console.WriteLine("EnterInsert_statement : {0}", context.GetText());
            base.EnterAlias_quoted_string(context);
        }

        public override void EnterBetween_bound([NotNull] PlSqlParser.Between_boundContext context)
        {
            Console.WriteLine("EnterBetween_bound : {0}", context.GetText());
            base.EnterBetween_bound(context);
        }



        public override void EnterBlock([NotNull] PlSqlParser.BlockContext context)
        {
            //Console.WriteLine("EnterBlock : {0}", context.GetText());

            base.EnterBlock(context);
        }

        public override void EnterBody([NotNull] PlSqlParser.BodyContext context)
        {
            //Console.WriteLine("EnterBody : {0}", context.GetText());
            
            base.EnterBody(context);
        }

        public override void ExitBody([NotNull] PlSqlParser.BodyContext context)
        {
            //Console.WriteLine("ExitBody : {0}", context.GetText());
            base.ExitBody(context);
        }

        public override void EnterCursor_name([NotNull] PlSqlParser.Cursor_nameContext context)
        {
            //Console.WriteLine("EnterCursor_name : {0}", context.GetText());
            base.EnterCursor_name(context);
        }


        public override void EnterCursor_loop_param([NotNull] PlSqlParser.Cursor_loop_paramContext context)
        {
            //Console.WriteLine("EnterCursor_loop_param : {0}", context.GetText());
            LogContext(context, true);
            base.EnterCursor_loop_param(context);
        }

        public override void ExitCursor_loop_param([NotNull] PlSqlParser.Cursor_loop_paramContext context)
        {
            //Console.WriteLine("ExitCursor_loop_param : {0}", context.GetText());
            LogContext(context, false, true);
            base.ExitCursor_loop_param(context);
        }

        public override void EnterLoop_statement([NotNull] PlSqlParser.Loop_statementContext context)
        {
            //Console.WriteLine("EnterLoop_statement : {0}", context.GetText());
            LogContext(context);
            this.pushCtxChild("Loop_statement", context);
            base.EnterLoop_statement(context);
        }

        public override void ExitLoop_statement([NotNull] PlSqlParser.Loop_statementContext context)
        {
            LogContext(context, false, true);
            this.popCtxChild();
            base.ExitLoop_statement(context);
        }

        public override void EnterLabel_declaration([NotNull] PlSqlParser.Label_declarationContext context)
        {
            //Console.WriteLine("EnterLabel_declaration : {0}", context.GetText());
            base.EnterLabel_declaration(context);
        }

        public override void EnterIndex_name([NotNull] PlSqlParser.Index_nameContext context)
        {
            //Console.WriteLine("EnterIndex_name : {0}", context.GetText());
            base.EnterIndex_name(context);
        }

        public override void EnterRecord_var_dec([NotNull] PlSqlParser.Record_var_decContext context)
        {
            LogContext(context, true);
            base.EnterRecord_var_dec(context);
        }

        public override void EnterRecord_declaration([NotNull] PlSqlParser.Record_declarationContext context)
        {
            LogContext(context, true);
            base.EnterRecord_declaration(context);
        }

        public override void EnterRecord_type_dec([NotNull] PlSqlParser.Record_type_decContext context)
        {
            LogContext(context, true);
            base.EnterRecord_type_dec(context);
        }

        public override void ExitRecord_declaration([NotNull] PlSqlParser.Record_declarationContext context)
        {
            LogContext(context, true, true);
            base.ExitRecord_declaration(context);
        }

        public override void ExitRecord_type_dec([NotNull] PlSqlParser.Record_type_decContext context)
        {
            LogContext(context, true, true);
            base.ExitRecord_type_dec(context);
        }

        public override void ExitRecord_var_dec([NotNull] PlSqlParser.Record_var_decContext context)
        {
            LogContext(context, true, true);
            base.ExitRecord_var_dec(context);
        }

        public override void EnterRecord_name([NotNull] PlSqlParser.Record_nameContext context)
        {

            LogContext(context, true);
            this.current_ctx_info.record_name = context;            
            
            base.EnterRecord_name(context);
        }



        public override void ExitRecord_name([NotNull] PlSqlParser.Record_nameContext context)
        {
            LogContext(context, true, true);
            
            base.ExitRecord_name(context);
        }

        public override void EnterLabel_name([NotNull] PlSqlParser.Label_nameContext context)
        {
            Console.WriteLine("EnterLabel_name : {0}", context.GetText());
            base.EnterLabel_name(context);
        }

        public override void EnterFlashback_query_clause([NotNull] PlSqlParser.Flashback_query_clauseContext context)
        {
            Console.WriteLine("EnterFlashback_query_clause : {0}", context.GetText());
            base.EnterFlashback_query_clause(context);
        }

        public override void EnterFunction_name([NotNull] PlSqlParser.Function_nameContext context)
        {
            Console.WriteLine("EnterFunction_name : {0}", context.GetText());
            base.EnterFunction_name(context);
        }

        public override void EnterCursor_expression([NotNull] PlSqlParser.Cursor_expressionContext context)
        {
            Console.WriteLine("EnterCursor_expression : {0}", context.GetText());
            base.EnterCursor_expression(context);
        }

        public override void EnterSql_cursor_expression([NotNull] PlSqlParser.Sql_cursor_expressionContext context)
        {
            Console.WriteLine("EnterSql_cursor_expression : {0}", context.GetText());
            base.EnterSql_cursor_expression(context);
        }

        public override void EnterCursor_manipulation_statements([NotNull] PlSqlParser.Cursor_manipulation_statementsContext context)
        {
            Console.WriteLine("EnterCursor_manipulation_statements : {0}", context.GetText());
            base.EnterCursor_manipulation_statements(context);
        }

        public override void EnterVariable_declaration([NotNull] PlSqlParser.Variable_declarationContext context)
        {
            Console.WriteLine("EnterVariable_declaration : {0}", context.GetText());
            base.EnterVariable_declaration(context);
        }



        public override void EnterVariable_name([NotNull] PlSqlParser.Variable_nameContext context)
        {


            if (!is_ignorable_variable(context))
            {
                Console.WriteLine("EnterVariable_name : {0}", context.GetText());
                // register variable
                current_ctx_info.variables.Add(context.GetText());

            } else
            {
                // Console.WriteLine("EnterVariable_name(ignored) : {0}", context.GetText());
            }

            base.EnterVariable_name(context);
        }

        public override void EnterDeclare_wrapper([NotNull] PlSqlParser.Declare_wrapperContext context)
        {
            Console.WriteLine("EnterDeclare_wrapper : {0}", context.GetText());
            base.EnterDeclare_wrapper(context);
        }



        public override void EnterDeclare_spec([NotNull] PlSqlParser.Declare_specContext context)
        {
            // Console.WriteLine("EnterDeclare_spec : {0}", context.GetText());
            base.EnterDeclare_spec(context);
        }

        public override void EnterSingle_column_for_loop([NotNull] PlSqlParser.Single_column_for_loopContext context)
        {
            Console.WriteLine("EnterSingle_column_for_loop : {0}", context.GetText());
            base.EnterSingle_column_for_loop(context);
        }

        public override void EnterMulti_column_for_loop([NotNull] PlSqlParser.Multi_column_for_loopContext context)
        {
            Console.WriteLine("EnterMulti_column_for_loop : {0}", context.GetText());
            base.EnterMulti_column_for_loop(context);
        }

        public override void EnterExpression([NotNull] PlSqlParser.ExpressionContext context)
        {
            if (!is_quoted_string(context))
            {
                if (!is_ignorable_expr(context))
                {
                    if (!is_ignorable_variable(context))
                    {
                        if (is_target_str(context))
                        {
                            Console.WriteLine("EnterExpression : {0}", context.GetText());
                            this.current_ctx_info.expr_fields.Add(context);
                        }
                    }
                }
            }

            base.EnterExpression(context);
        }

        /*
        public override void EnterLogical_and_expression([NotNull] PlSqlParser.Logical_and_expressionContext context)
        {
            if (!is_quoted_string(context))
            {
                if (!is_ignorable_expr(context))
                {
                    if (!is_ignorable_variable(context))
                    {
                        Console.WriteLine("EnterLogical_and_expression : {0}", context.GetText());
                    }
                }
            }

            base.EnterLogical_and_expression(context);
        }

        public override void EnterMultiset_expression([NotNull] PlSqlParser.Multiset_expressionContext context)
        {
            if (!is_ignorable_expr(context))
            {
                Console.WriteLine("EnterMultiset_expression : {0}", context.GetText());
            }

            base.EnterMultiset_expression(context);
        }

        public override void EnterRelational_expression([NotNull] PlSqlParser.Relational_expressionContext context)
        {
            if (!is_ignorable_expr(context))
            {
                Console.WriteLine("EnterRelational_expression : {0}", context.GetText());
            }

            base.EnterRelational_expression(context);
        }

        public override void EnterBetween_elements([NotNull] PlSqlParser.Between_elementsContext context)
        {
            Console.WriteLine("EnterBetween_elements : {0}", context.GetText());
            base.EnterBetween_elements(context);
        }

        public override void EnterConcatenation([NotNull] PlSqlParser.ConcatenationContext context)
        {
            base.EnterConcatenation(context);
        }
        
        */

        

        
    }


    
    
}
