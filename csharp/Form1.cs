﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using analyzer;
using Antlr4.Runtime;
using System.IO;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;
/// <summary>
/// 
/// 
/// </summary>
namespace analyzer
{
    public partial class Form1 : Form
    {

        public ColumnDesc desc_table = new ColumnDesc();

        public Form1()
        {
            InitializeComponent();
            // desc_table.load_description(@"C:\Work\Other\antlr4-oracle\csharp\bin\Debug\column_explain.xlsx");
            // Console.Write("Done load_description.");
            desc_table.load_mapping(@"C:\Work\Other\antlr4-oracle\csharp\bin\Debug\table_sim.xlsx");
            Console.Write("Done load_mapping.");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dr = this.openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                this.openFileDialog1.OpenFile();

            }
        }

        private void conv_with_ctx(BkListener lsnr, ContextInfo ctx_info, TokenStreamRewriter rewriter)
        {
            Console.WriteLine(ctx_info.ctx_stmt);

            if (ctx_info.record_name != null)
            {
                Console.WriteLine("Record {0} exists.", ctx_info.record_name.GetText());

            }
            
            // 1. column conversion
            foreach (object col in ctx_info.columns)
            {
                ParserRuleContext col_name = col as ParserRuleContext;

                string str_col = col_name.GetText().ToUpper();
                if (str_col.Contains("anlys_cmp_id"))
                {
                    Console.WriteLine("TEST");
                }

                if (str_col.Contains("."))
                {
                    // aliased column
                    Console.WriteLine("Alias. {0}", str_col);
                }
                else
                {
                    // no alias
                    foreach (DmlTable tab in ctx_info.tables)
                    {
                        string k = tab.table_name.GetText().ToUpper() + "." + str_col;
                        if (desc_table.dic_table.ContainsKey(k))
                        {
                            var o = desc_table.dic_table[k];
                            rewriter.Replace(col_name.Start, o.columns[str_col].to_be_column_name);
                        }
                    }
                }
            }


            // 2. 'Table' conversion
            foreach (DmlTable table in ctx_info.tables)
            {
                string k = table.table_name.GetText().ToUpper();
                if (!desc_table.table_asis_tobe.ContainsKey(k))
                {
                    rewriter.Replace(table.table_name.stop, "/* " + k + " */");
                    continue;
                }

                List<string> tobe = desc_table.table_asis_tobe[k];
                string tabname = tobe.First();
                rewriter.Replace(table.table_name.stop, tabname);
            }


            // 3. 'Expression' conversion.
            foreach (ParserRuleContext expr in ctx_info.expr_fields)
            {
                Console.WriteLine("expr : {0}", expr.GetText());
                if (expr.GetText().Contains("anlys_cmp_id"))
                {
                    Console.WriteLine("TEST");
                }

                if (Util.is_aliased_expr(expr))
                {
                    string alias = expr.start.Text.ToUpper();
                    bool is_found = false;
                    foreach (DmlTable table in ctx_info.tables)
                    {
                        string t_name = table.table_name.GetText().ToUpper();
                        string a_name = table.table_alias == null ? "##" : table.table_alias.GetText().ToUpper();

                        if (a_name.Equals(alias))
                        {
                            if (desc_table.dic_table.ContainsKey(t_name + "." + expr.stop.Text.ToUpper()))
                            {
                                var o = desc_table.dic_table[t_name + "." + expr.stop.Text.ToUpper()];
                                var p = o.columns[expr.stop.Text.ToUpper()].to_be_column_name;

                                rewriter.Replace(expr.stop, p);
                            }
                        }
                    }


                    if (is_found) 
                    {

                    }
                    else
                    {
                        // table is not found.

                    }
                }
                else
                {
                    foreach (DmlTable tab in ctx_info.tables)
                    {
                        string k = tab.table_name.GetText().ToUpper();
                        if (desc_table.dic_table.ContainsKey(k + "." + expr.GetText().ToUpper()))
                        {
                            var o = desc_table.dic_table[k + "." + expr.GetText().ToUpper()];
                            var p = o.columns[expr.GetText().ToUpper()].to_be_column_name;

                            rewriter.Replace(expr.Start, p);
                        }
                    }
                }

            }

            // 4. 'Parameter' conversion
            if (ctx_info.parameters != null)
            {
                foreach (object para in ctx_info.parameters)
                {
                    string para_name = para as string;
                    Console.WriteLine("Param : ", para_name);
                }
            }



            // 6. call recursive for children
            foreach (ContextInfo c_info in ctx_info.children)
            {
                conv_with_ctx(lsnr, c_info, rewriter);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            // 변경일자/2018-03-01
            // 비고 /유지/삭제
            // As-Is 테이블명/SK_TH_ERP_P2P
            // As-Is 컬럼ID/UNIT_1
            // To-Be 테이블명/TB_ERP_RESULT_DTL
            // To-Be 컬럼ID/ERP_MVMT_UNIT_ID
            // 비고/TSH_IF_SNDTSH_IF_LOG
            


            string inputFile = null;
            var istream = System.Console.In;

            if (richTextBox1.Text.Trim() == string.Empty)
            {
                // Very Large File
                // inputFile = @"C:\Work\Other\antlr4-oracle\csharp\bin\Debug\pkg3.pkb";
                inputFile = @"C:\Work\Other\antlr4-oracle\csharp\bin\Debug\test1.prc";
                var txt = File.ReadAllText(inputFile, Encoding.GetEncoding("utf-8"));
                richTextBox1.Text = txt;

            } else
            {
                richTextBox1.Text = richTextBox1.Text.Replace("\n", Environment.NewLine);
            }

            


            string target_text = richTextBox1.Text;
            if (target_text.Contains("DROP VIEW"))
            {
                target_text = target_text.Replace("DROP VIEW", "--DROP VIEW");
            }

            // byte[] byteArray = Encoding.ASCII.GetBytes(textBox2.Text);
            byte[] byteArray = Encoding.UTF8.GetBytes(target_text);


            var tstream = new MemoryStream(byteArray);

            var input = new AntlrInputStream(tstream);
            var lexer = new PlSqlLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new PlSqlParser(tokens);
            
            var tree = parser.sql_script();
            TokenStreamRewriter rewriter = new TokenStreamRewriter(tokens);

    
            //var visitor = new BkVisitor<string>();
            //visitor.Visit(tree);


            ParseTreeWalker walker = new ParseTreeWalker();
            BkListener lsnr = new BkListener(parser, rewriter);
            walker.Walk(lsnr, tree);

            Console.WriteLine(">> Parsing completed.");

            ContextInfo ctx_info = lsnr.current_ctx_info;
            this.conv_with_ctx(lsnr, ctx_info, rewriter);


            //foreach (var tk in tokens.GetTokens())
            //{
            //    if (tk.Type == 536)  // single, multiline comment --> 537
            //    {
            //        rewriter.Replace(tk, string.Empty);
            //    }
            //}



            //foreach (TokenInfo atok in visitor.column_token_id_list)
            //{

            //    for (int i = atok.interval.a; i <= atok.interval.b; i++)
            //    {
            //        var tok = tokens.Get(i);

            //        rewriter.Replace(tokens.Get(i), "\r\n" + tokens.Get(i).Text);
            //        break;
            //    }
            //}


            // TokenSource = PlSqlLexer

            StringBuilder sb = new StringBuilder();


            foreach (var tk in tokens.GetTokens())
            {
                
                //if (tk.Type == 536)  // single, multiline comment --> 537
                //{
                //    Console.Write(tk.Type);
                //    Console.Write("<==>");
                //    Console.WriteLine(tk.Text);
                //    tk.Text.Replace("--", "/*");
                //    rewriter.Replace(tk, tk.Text.Replace("--", "/*"));
                //}

                //if (tk.Type == 630)
                //{
                //    Console.WriteLine("=====>Table : {0}", tk.Text);

                //}

                //if (tk.Text.ToUpper().Equals("TEST_FN"))
                //{
                //    rewriter.Replace(tk, "__post__");
                //}

                //if (tk.Text.ToUpper().Equals("V_REF_COL"))
                //{
                //    rewriter.Replace(tk, "SELECT V_COL FROM REF_TABLE");
                //}

                //string table_to_conv = null;
                //string col_to_conv = null;

                //if (dic_table.ContainsKey(tk.Text.ToUpper()))
                //{
                //    table_to_conv = dic_table[tk.Text.ToUpper()];
                //}

                //if (dic_col.ContainsKey(tk.Text.ToUpper()))
                //{
                //    col_to_conv = dic_col[tk.Text.ToUpper()];
                //}

                //if (table_to_conv != null)
                //{
                //    rewriter.Replace(tk, table_to_conv);
                //}

                //if (col_to_conv != null)
                //{
                //    rewriter.Replace(tk, col_to_conv);
                //}

            }

            //foreach (var tk in tokens.GetTokens())
            //{

            //    sb.Append(tk.Text);
            //}

            var txt2 = rewriter.GetText();

            //var txt3 = sb.ToString();

            if (txt2.Contains("--DROP VIEW"))
            {
                txt2 = txt2.Replace("--DROP VIEW", "DROP VIEW");
            }

            richTextBox2.Text = txt2;

            //var token = lexer.NextToken();
            //Console.WriteLine(token.Text);
            //Console.WriteLine(token.Line);    
        }

        enum SELECT_STATE
        {
            NOT_IN_SELECT,
            SELECT_FIRST_LINE_BEFORE_FIRST_COLUMN,
            SELECT_FIRST_LINE_IN_FIRST_COLUMN,
            SELECT_NEXT_LINE,
            INTO_STARTED,
            FROM_STARTED,
            WHERE_STARTED,
            SELECT_FIRST_LINE_IN_FIRST_COLUMN_OPEN,
            SELECT_FIRST_LINE_IN_FIRST_COLUMN_CLOSED,
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // byte[] byteArray = Encoding.ASCII.GetBytes(textBox2.Text);
            byte[] byteArray = Encoding.UTF8.GetBytes(richTextBox2.Text);
            var tstream = new MemoryStream(byteArray);

            var input = new AntlrInputStream(tstream);
            var lexer = new PlSqlLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new PlSqlParser(tokens);
            var tree = parser.sql_script();

            int current_column = 0;
            bool is_first_column = true;

            SELECT_STATE state = SELECT_STATE.NOT_IN_SELECT;

            int select_pos = 0;
            int id_pos = 0;
            int select_open_depth = 0;
            StringBuilder sb = new StringBuilder();


            foreach (IToken token in tokens.GetTokens())
            {
                Console.WriteLine("{0}/{1}/", token.Type, token.Text);
                string str_to_append = token.Text;



                switch (token.Type)
                {
                    case 119: // 'DROP' command
                        break;

                    case 535: // SPACES
                        break;

                    case 349: // SELECT
                        select_pos = token.Column;
                        select_open_depth = 0;
                        state = SELECT_STATE.SELECT_FIRST_LINE_BEFORE_FIRST_COLUMN;

                        break;

                    case 8:     // A_LETTER  
                    case 630:   // ID
                        switch (state)
                        {
                            case SELECT_STATE.SELECT_FIRST_LINE_BEFORE_FIRST_COLUMN:
                                id_pos = token.Column;
                                state = SELECT_STATE.SELECT_FIRST_LINE_IN_FIRST_COLUMN;
                                break;

                            case SELECT_STATE.SELECT_FIRST_LINE_IN_FIRST_COLUMN:
                                // no state change.
                                break;

                            case SELECT_STATE.SELECT_NEXT_LINE:
                                break;
                        }
                        
                        break;

                    case 514: // COMMA - ','
                        switch (state)
                        {
                            case SELECT_STATE.SELECT_FIRST_LINE_IN_FIRST_COLUMN:
                                if (select_open_depth == 0) { 
                                    str_to_append += Environment.NewLine + new string(' ', id_pos - 1);
                                    state = SELECT_STATE.SELECT_NEXT_LINE;
                                }
                                break;

                            case SELECT_STATE.SELECT_NEXT_LINE:
                                if (select_open_depth == 0)
                                {
                                    str_to_append += Environment.NewLine + new string(' ', id_pos - 1);
                                    state = SELECT_STATE.SELECT_NEXT_LINE;
                                }
                                break;

                        }
                        
                        break;

                    case 192: // INTO
                        break;

                    case 508: // (
                        switch (state)
                        {
                            case SELECT_STATE.SELECT_FIRST_LINE_IN_FIRST_COLUMN:
                                select_open_depth++;
                                break;
                        }
                        break;

                    case 509: // )
                        switch (state)
                        {
                            case SELECT_STATE.SELECT_FIRST_LINE_IN_FIRST_COLUMN:
                                select_open_depth--;
                                break;
                        }
                        break;

                    case 520: // ;
                        state = SELECT_STATE.NOT_IN_SELECT;
                        break;

                        

                    default:
                        break;
                }

                sb.Append(str_to_append);
            }

            Console.WriteLine("Done btfying.");
            richTextBox2.Text = sb.ToString();

        }
    }
}
