﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antlr4.Runtime;
using System.Text.RegularExpressions;

namespace analyzer
{
    public static class Util
    {
        public static bool is_simple_name(ParserRuleContext context)
        {
            Regex regexString = new Regex(@"^[a-zA-Z0-9\\_]+$");
            string item = context.GetText();
            return regexString.IsMatch(item);

        }

        public static bool is_aliased_expr(ParserRuleContext context)
        {
            Regex regexString = new Regex(@"^[a-zA-Z0-9\\_]+[.][a-zA-Z0-9\\_]+$");
            string item = context.GetText();
            return regexString.IsMatch(item);

        }
    }
}
