﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime;

namespace analyzer
{
    public class BkVisitor<Result> : PlSqlBaseVisitor<Result>
    {
        PlSqlParser parser;
        TokenStreamRewriter rewriter;
        ColumnDesc dict = null;

        Stack<int> position_queue = new Stack<int>();
        Stack<ContextInfo> context_queue = new Stack<ContextInfo>();


        //public List<String> table_list = new List<string>();
        //public List<TokenInfo> column_token_id_list = new List<TokenInfo>();

        public override Result VisitCreate_package([NotNull] PlSqlParser.Create_packageContext context)
        {
            Console.WriteLine(context.GetText());
            return base.VisitCreate_package(context);
        }

        

        public override Result VisitVariable_declaration([NotNull] PlSqlParser.Variable_declarationContext context)
        {
            var r = context.GetText();
            context.variable_name().GetText();

            return base.VisitVariable_declaration(context);
        }

        public override Result VisitProcedure_name([NotNull] PlSqlParser.Procedure_nameContext context)
        {
            return base.VisitProcedure_name(context);
        }

        public override Result VisitBody([NotNull] PlSqlParser.BodyContext context)
        {
            return base.VisitBody(context);
        }

        RuleContext current_sql_stmt_context = null;
        public override Result VisitSql_statement([NotNull] PlSqlParser.Sql_statementContext context)
        {

            return base.VisitSql_statement(context);
        }

        public override Result VisitTable_ref_list([NotNull] PlSqlParser.Table_ref_listContext context)
        {
            return base.VisitTable_ref_list(context);
        }

        public override Result VisitTable_ref([NotNull] PlSqlParser.Table_refContext context)
        {
            return base.VisitTable_ref(context);
        }

        public override Result VisitTableview_name([NotNull] PlSqlParser.Tableview_nameContext context)
        {
            return base.VisitTableview_name(context);
        }

        public override Result VisitTable_alias([NotNull] PlSqlParser.Table_aliasContext context)
        {
            return base.VisitTable_alias(context);
        }

        public override Result VisitTable_ref_aux([NotNull] PlSqlParser.Table_ref_auxContext context)
        {
            return base.VisitTable_ref_aux(context);
        }



        /// <summary>
        /// get table name from expression.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override Result VisitDml_table_expression_clause([NotNull] PlSqlParser.Dml_table_expression_clauseContext context)
        {

            return base.VisitDml_table_expression_clause(context);
        }


        public override Result VisitSelect_list_elements([NotNull] PlSqlParser.Select_list_elementsContext context)
        {
            return base.VisitSelect_list_elements(context);
        
        }


        public override Result VisitSelected_tableview([NotNull] PlSqlParser.Selected_tableviewContext context)
        {
            Console.WriteLine("VisitSelected_tableview : {0}", context.GetText());
            return base.VisitSelected_tableview(context);
        }

        public override Result VisitColumn_name([NotNull] PlSqlParser.Column_nameContext context)
        {
            return base.VisitColumn_name(context);
        }

        public override Result VisitSelect_statement([NotNull] PlSqlParser.Select_statementContext context)
        {
            var query_block = context.subquery().subquery_basic_elements().query_block();

            Console.WriteLine("VisitSelect_statement : {0}", context.GetText());
            Console.WriteLine("    subquery : {0}", context.subquery().GetText());

            foreach (var selected in query_block.selected_element())
            {
                Console.WriteLine("        selected_element : {0}", selected.GetText());
            }

            foreach (var tab in query_block.from_clause().table_ref_list().table_ref())
            {
                Console.WriteLine("        from : {0}, {1}",
                    tab.table_ref_aux().dml_table_expression_clause().GetText(),
                    tab.table_ref_aux().table_alias() == null ? "null" : tab.table_ref_aux().table_alias().GetText()
                    );
            }

            var where_clause = query_block.where_clause();
            if (where_clause != null) { 
                Console.WriteLine("        where : {0}", query_block.where_clause().condition_wrapper().expression().GetText());
                foreach (var expr in query_block.where_clause().condition_wrapper().expression().logical_and_expression().negated_expression())
                {
                    foreach (var comp_expr in expr.equality_expression().multiset_expression().relational_expression().compound_expression())
                    {
                        Console.WriteLine("          expr : {0}", comp_expr.GetText());
                    }

                }
            }

            Console.WriteLine("return VisitSelect_statement : {0}", context.GetText());
            return base.VisitSelect_statement(context);
        }


        

        public override Result VisitFrom_clause([NotNull] PlSqlParser.From_clauseContext context)
        {
            return base.VisitFrom_clause(context);
        }

        
        public override Result VisitTable_element([NotNull] PlSqlParser.Table_elementContext context)
        {
            Console.WriteLine("VisitTable_element : {0}", context.GetText());
            return base.VisitTable_element(context);
        }

        public override Result VisitAlias_quoted_string([NotNull] PlSqlParser.Alias_quoted_stringContext context)
        {
            Console.WriteLine("VisitAlias_quoted_string : {0}", context.GetText());
            return base.VisitAlias_quoted_string(context);
        }


        public override Result VisitUpdate_set_clause([NotNull] PlSqlParser.Update_set_clauseContext context)
        {
            return base.VisitUpdate_set_clause(context);
        }

        public override Result VisitInsert_statement([NotNull] PlSqlParser.Insert_statementContext context)
        {
            return base.VisitInsert_statement(context);
        }

        public override Result VisitInsert_into_clause([NotNull] PlSqlParser.Insert_into_clauseContext context)
        {
            return base.VisitInsert_into_clause(context);
        }

        public override Result VisitDelete_statement([NotNull] PlSqlParser.Delete_statementContext context)
        {
            return base.VisitDelete_statement(context);
        }
    }
}
