﻿using Antlr4.Runtime;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace analyzer
{
    
    public class DmlTable
    {
        
        public ParserRuleContext table_name;
        public ParserRuleContext table_alias;
       //  public List<string> columns;


    }

    public class SelectedElement
    {
        public ParserRuleContext selected_element;
        public List<ParserRuleContext> expressions;
        public ParserRuleContext column_alias;
    }

    public class ContextInfo
    {
        public string ctx_stmt;
        // recursive ContextInfo
        public ArrayList children;
        public ContextInfo parent;
        public string ctx_text;
        public ArrayList columns;
        public ArrayList tables;
        public ArrayList expr_fields;
        public List<string> variables;
        public List<string> parameters;

       
        // record_name is only for "loop statement"
        public ParserRuleContext record_name;      

        public ArrayList selected_elements;
            
        public ContextInfo(string ctx_stmt)
        {

            this.ctx_stmt = ctx_stmt;
            this.parent = null;
            children = new ArrayList();
            columns = new ArrayList();
            tables = new ArrayList();
            expr_fields = new ArrayList();
            variables = new List<string>();
            
            selected_elements = new ArrayList();

        }

    }
}
