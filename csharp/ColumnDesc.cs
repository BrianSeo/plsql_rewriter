﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using OfficeOpenXml.Style;
using OfficeOpenXml;

namespace analyzer
{
    public class ColumnConv
    {
        public string as_is_column_name;
        public string to_be_table_name;
        public string to_be_column_name;
    }

    public class TableConv
    {
        public string as_is_table_name;
        public string to_be_table_name;
        public Dictionary<string, ColumnConv> columns = new Dictionary<string, ColumnConv>();
    }

    public class ColumnDesc
    {
        
        // Dictionary<string, string> dict_desc = new Dictionary<string, string>();

        public Dictionary<string, TableConv> dic_table = new Dictionary<string, TableConv>();
        public Dictionary<string, List<string>> table_asis_tobe = new Dictionary<string, List<string>>();


        public void load_mapping(string filename)
        {
            FileInfo existingFile = new FileInfo(filename);
            using (ExcelPackage package = new ExcelPackage(existingFile))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

                int row = 2;
                

                while (worksheet.Cells[row, 1].Value != null) // 더이상의 레코드가 없으면 종료.
                {
                    string as_is_table = string.Empty; 
                    string as_is_column = "#UNDEFINED#";
                    string to_be_table = "#REMOVED_TABLE#";
                    string to_be_column = "#REMOVED_COLUMN#";

                    if (worksheet.Cells[row, 3].Value != null) 
                    {
                        // 현재 테이블명
                        as_is_table = worksheet.Cells[row, 3].Value.ToString();
                    }

                    if (worksheet.Cells[row, 4].Value != null)
                    {
                        // 현재 컬럼명
                        as_is_column = worksheet.Cells[row, 4].Value.ToString();
                    }

                    if (worksheet.Cells[row, 5].Value != null)
                    {
                        // To-Be 테이블명
                        to_be_table = worksheet.Cells[row, 5].Value.ToString();
                    }

                    if (worksheet.Cells[row, 6].Value != null)
                    {
                        // To-Be 컬럼
                        to_be_column = worksheet.Cells[row, 6].Value.ToString();
                    }

                    if (!to_be_table.Equals("#REMOVED_TABLE#"))
                    {
                        if (table_asis_tobe.ContainsKey(as_is_table))
                        {
                            var list = table_asis_tobe[as_is_table];
                            bool is_ignorable = list.Any(to_be_table.Equals);
                            if (!is_ignorable)
                            {
                                list.Add(to_be_table);
                                Console.WriteLine("Table is separated : {0}, {1}", as_is_table, to_be_table);
                            }
                        }
                        else
                        {
                            List<string> tobe_list = new List<string>();
                            tobe_list.Add(to_be_table);
                            table_asis_tobe.Add(as_is_table, tobe_list);
                        }
                    }

                    if (!dic_table.ContainsKey(as_is_table + "." + as_is_column))
                    {
                        // 로드안된 테이블일 경우 추가.
                        TableConv tc = new TableConv();
                        tc.as_is_table_name = as_is_table;

                        ColumnConv cc = new ColumnConv();
                        cc.as_is_column_name = as_is_column;
                        cc.to_be_table_name = to_be_table;
                        cc.to_be_column_name = to_be_column;
                        tc.columns.Add(cc.as_is_column_name, cc);

                        dic_table.Add(as_is_table + "." + as_is_column, tc);
                    } 
                    else
                    {
                        TableConv tc = dic_table[as_is_table + "." + as_is_column];
                        if (tc.columns.ContainsKey(as_is_column))
                        {
                            Console.WriteLine("As-is column dup! : {0}.{1}", as_is_table, as_is_column);
                        }
                        else
                        {
                            ColumnConv cc = new ColumnConv();
                            cc.as_is_column_name = as_is_column;
                            cc.to_be_table_name = to_be_table;
                            cc.to_be_column_name = to_be_column;
                            tc.columns.Add(cc.as_is_column_name, cc);
                        }
                    }


                    row++;
                }

            } // the using statement automatically calls Dispose() which closes the package.

            Console.WriteLine("Loaded 'To Be' Mapping =================");
        }
        public void load_description(string filename)
        {
            //FileInfo existingFile = new FileInfo(filename);
            //using (ExcelPackage package = new ExcelPackage(existingFile))
            //{
            //    // get the first worksheet in the workbook
            //    ExcelWorksheet worksheet = package.Workbook.Worksheets[1];

            //    int row = 1;
            //    while (worksheet.Cells[row, 1].Value != null)
            //    {
            //        // Console.WriteLine(worksheet.Cells[row, col].Value.ToString());
            //        string key = worksheet.Cells[row, 4].Value.ToString() + worksheet.Cells[row, 6].Value.ToString();
            //        if (!dict_desc.ContainsKey(key))
            //        {
            //            dict_desc.Add(key, worksheet.Cells[row, 5].Value.ToString());
            //        }
            //        else
            //        {
            //            Console.WriteLine("Key Dup: {0},{1}", worksheet.Cells[row, 4].Value.ToString(), worksheet.Cells[row, 6].Value.ToString());
            //        }

            //        row++;
            //    }
                
            //}

            Console.WriteLine("Loaded Column Description =================");
        }

        public string get_culumn_desc(string tname, string cname)
        {
            string desc = "Not Found";
            //string concat_key = tname + cname;
            //if (dict_desc.ContainsKey(concat_key))
            //{
            //    desc = dict_desc[concat_key];
            //}

            return desc;
        }

        public string get_tobe_table(string asis_table)
        {
            return string.Empty;
        }

        public string get_tobe_column(string asis_table, string asis_column)
        {
            return string.Empty;
        }

    }
}
