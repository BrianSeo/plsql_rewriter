﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Antlr4.Runtime;
using Antlr4.Runtime.Misc;
using Antlr4.Runtime.Tree;

using Antlr4.Runtime.Atn;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace analyzer
{
	class Program
	{

       
		public static void analyze(TextReader file) {
			var input = new AntlrInputStream(file);
			var lexer = new PlSqlLexer(input);
			var tokens = new CommonTokenStream(lexer);
			var parser = new PlSqlParser(tokens);
			var tree = parser.sql_script();

            ParseTreeWalker walker = new ParseTreeWalker();
            TokenStreamRewriter rewriter = new TokenStreamRewriter(tokens);

            Console.WriteLine(rewriter.GetText());

            rewriter.Replace(2, " converted hello");

            Console.WriteLine(rewriter.GetText());

		}

        public static void analyze_2(TextReader file)
        {
            // 변경일자/2018-03-01
            // 비고 /유지/삭제
            // As-Is 테이블명/SK_TH_ERP_P2P
            // As-Is 컬럼ID/UNIT_1
            // To-Be 테이블명/TB_ERP_RESULT_DTL
            // To-Be 컬럼ID/ERP_MVMT_UNIT_ID
            // 비고/TSH_IF_SNDTSH_IF_LOG
            var input = new AntlrInputStream(file);
            var lexer = new PlSqlLexer(input);
            var tokens = new CommonTokenStream(lexer);
            var parser = new PlSqlParser(tokens);
            var tree = parser.sql_script();

            ParseTreeWalker walker = new ParseTreeWalker();
            TokenStreamRewriter rewriter = new TokenStreamRewriter(tokens);

            var tok = tokens.Get(0);
            Console.WriteLine(tok.Text);

            tok = tokens.Get(2);
            Console.WriteLine(tok.Text);

            // TokenSource = PlSqlLexer
            foreach (var tk in tokens.GetTokens())
            {
                Console.Write(tk.Type);
                Console.Write("<==>");
                Console.WriteLine(tk.Text);
            }

            //var token = lexer.NextToken();
            //Console.WriteLine(token.Text);
            //Console.WriteLine(token.Line);

        }

        public static void Main_P(string[] args)
		{
			string inputFile = null;
			
			if (args.Length > 0) {
				inputFile = args[0];
				Console.WriteLine("Parsing " + inputFile);
			}
			var istream = System.Console.In;
			
			if (inputFile != null) {
				istream = File.OpenText(inputFile);
			}
			
			analyze_2(istream);
		}

        public static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
	}
}